export default [
    {
        path: '/',
        name: 'home',component: () => import(/* webpackChunkName: "about" */ '../views/Home')
    },
    {
      path: '/principal',
      name: 'principal',component: () => import('../views/Principal')
    },
    {
      path: '/professores',
      name: 'professores',component: () => import('../views/professor/Professores')
    },
    {
      path: '/professor/:id',
      name: 'professores-edit',component: id => import('../views/professor/Professores-edit')
    },
    {
      path: '/professor-remove/:id',
      name: 'professores-remove',component: id => import('../views/professor/Professores-remove')
    },
    {
      path: '/adicionar-professor',
      name: 'adicionar-professor',component: () => import('../views/professor/Professores-add')
    },
    {
      path: '/cursos',
      name: 'cursos',component: () => import('../views/cursos/Cursos')
    },
    {
      path: '/curso/:id',
      name: 'cursos-edit',component: id => import('../views/cursos/Cursos-edit')
    },
    {
      path: '/curso-remover/:id',
      name: 'curso-remove',component: id => import('../views/cursos/Cursos-remove')
    },
    {
      path: '/adicionar-curso',
      name: 'adicionar-curso',component: () => import('../views/cursos/Cursos-add')
    },
    {
      path: '/alunos',
      name: 'alunos',component: () => import('../views/alunos/Alunos')
    },
    {
      path: '/adicionar-aluno',
      name: 'adicionar-aluno',component: () => import('../views/alunos/Alunos-add')
    },
    {
      path: '/aluno/:id',
      name: 'aluno-edit',component: id => import('../views/alunos/Alunos-edit')
    },
    {
      path: '/aluno-remover/:id',
      name: 'aluno-remove',component: id => import('../views/alunos/Alunos-remove')
    },
    {
      path: '/pdf',
      name: 'pdf',component: () => import('../views/Pdf')
    },
    {
      path: '/logout',
      name: 'logout',component: () => import('../views/Logout')
    },
    {
        path: '/criar-conta',
        name: 'criar-conta',component: () => import(/* webpackChunkName: "about" */ '../views/Registro')
    },
]
