import Vue from 'vue'
import Vuex from 'vuex'
import Aluno from './aluno'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    alunoList: [],
  },
  mutations: {
    updateAccountList (state, data) {
        state.accountList = data
      }
  },
  actions: {
        getAcounts (context){
            Vue.http.get('aluno').then(response => {
                context.commit('updateAccountList', response.data)
            })
        }
  }
})
