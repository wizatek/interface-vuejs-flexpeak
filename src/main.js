
import Vue from 'vue'
import App from './App'
import store from './store/store.js'
import router from './router'
import LoginInterceptors from './modules/auth/interceptors'
import ViaCep from 'vue-viacep'
Vue.config.productionTip = false


Vue.use(ViaCep);
window._Vue = new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')


LoginInterceptors.check_auth(router)
LoginInterceptors.check_empty_token(router)